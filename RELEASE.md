# Release process

The release process consists on bumping the release version in the `meson.build` file and creating a new tag with the version.

# Upload packages to Pypi

The distribution sources and the pre-built wheels are manually uploaded to Pypi.

1. Locate the pipeline for the published tag.
2. Download the pipeline artifacts for the jobs `wheel linux`, `wheel windows`, `wheel macos` and `sdist` into a folder named `wheelhouse`.
3. Upload the files the `gst-indent` project in Pypi using `twine`:

```
python3 -m twine upload --repository gst-indent wheelhouse/*
```

This process will be automated in the future to be done from the CI when a new tag is created.
